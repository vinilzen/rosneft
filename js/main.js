var fulePrices = {};

$(function() {

	// Set price
	$('.jq-fuel').each(function(){
		var $thisFuel = $(this).find('.jq-fuel-price');
		var fuelName = $(this).attr('data-name');
		var price = $thisFuel.val();

		fulePrices[fuelName] = [
			Math.floor(price/10),
			Math.floor(price%10),
			Math.floor(price*10%10),
			Math.floor(price*100%10)
		]
	});

	// Fill numbers
	$('.cell-unit .cell-numbers').each(function(){
		for (i=0; i<10; i++) {
			$(this).append('<li>'+i+'</li>')
		}
	});
	
	$(".lightSlider").each(function(){
		var $self = $(this),
			$lightSlider = $(this).lightSlider({
				item: 1,
				vertical: true,
				verticalHeight: 120,
				gallery: false,
				controls: false,
				pager: false,
				slideMargin: 0,
				enableTouch: true,
				loop: true,
				onSliderLoad: function(e){
					var unit = $self.attr('data-unit');
					var fuelName = $self.attr('data-fuel');
					$lightSlider.goToSlide(fulePrices[fuelName][unit-1]+1)
				},
				onAfterSlide: function(){
					var unit = $self.attr('data-unit');
					var fuelName = $self.attr('data-fuel');

					fulePrices[fuelName][unit-1] = $lightSlider.getCurrentSlideCount()-1;

					$('.'+fuelName+' .jq-fuel-price')
						.val(fulePrices[fuelName][0]+''+fulePrices[fuelName][1]+'.'+fulePrices[fuelName][2]+''+fulePrices[fuelName][3]);

				}
			});
	});

	$('#f_price').click(function(){
		var prices = {};

		// сделаем чтобы цены были не по цифре отдельно а номальным значением
		Object.keys(fulePrices).forEach(function(key){
			prices[key] = parseInt(fulePrices[key].join(''))/100;
		});

		$.ajax({
			method: "GET",
			url: "http://localhost:82/f_price",
			data: prices
		})
		.done(function() {
			alert( "Data Saved" );
		});
	});
});