'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var compass = require('compass-importer')
var pug = require('gulp-pug');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

// Static server
gulp.task('server', ['sass', 'pug'], function() {
    browserSync.init({
        reloadDelay: 500,
        notify: false,
        open: false,
        server: {
            baseDir: "./"
        },
        ghostMode: false
    });
    
	gulp.watch("./*.pug", ['pug']);
    gulp.watch("./scss/*.scss", ['sass']);
    gulp.watch("./index.html", reload);
});


gulp.task('pug', function() {
  return gulp.src('./index.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('./'));
});

gulp.task('sass', function() {
    return gulp.src('./scss/main.scss')
        // .pipe(sourcemaps.init())
        .pipe(sass({
        	importer: compass,
            outputStyle: 'compressed',
        }).on('error', sass.logError))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('./css'))
        .pipe(browserSync.stream());
});

gulp.task('default', ['server']);