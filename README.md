Слайдер для установки цен на топливо

Актуальные цены доступны в глобальной переменной `fuelPrices`:

```javascript
> fuelPrices
{fuel-92: Array(4), fuel-95: Array(4), fuel-98: Array(4), fuel-dt: Array(4)}
fuel-92: [3, 4, 9, 2]
fuel-95: [3, 4, 0, 3]
fuel-98: [3, 4, 9, 5]
fuel-dt: [3, 4, 8, 2]
```

Также цена находиться в скрытом поле, и доступна в виде строки:

```javascript
// .fuel-92 | .fuel-95 | .fuel-98 | .fuel-dt
> $('.fuel-92 .jq-fuel-price').val()
"34.92"
```